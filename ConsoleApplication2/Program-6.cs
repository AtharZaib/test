﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace mulaDfaVerification
{
    class Program
    {
        static void Main(string[] args)
        {
           

            //  implementing the Float DFA.

           

            int[,] float_tt = { {1,5,5,5,4,5,6 }, {5,2,2,5,5,5,5 }, { 5,5,5,3,5,5,5}, { 5,5,5,5,4,5,6}, {5,5,5,5,4,5,6 }, { 5,5,5,5,5,5,5}


                ,{5,5,5,5,7,5,5 }, {5,5,5,5,7,8,5 }, {5,9,9,5,10,5,5 }, {5,5,5,5,10,5,5 }, {5,5,5,5,10,5,5 }

            };

            int IS = 0;
            int[] float_FS = { 7, 10 };

            string[] float_chars = { "^({)$", "^([-])$", "^([+])$", "^(})$", "^([0-9])$", "^(e)$", "^(\\.)$" };

            FA float_automata = new FA(float_tt, IS, float_FS, float_chars);

            

       
            // Implementing Integer DFA!

            int[,] int_tt = { { 1, 5, 5, 5, 4 }, { 5, 5, 2, 2, 5 }, { 5, 3, 5, 5, 5 }, { 5, 5, 5, 5, 4 }, { 5, 5, 5, 5, 4 }, { 5, 5, 5, 5, 5 } };
            IS = 0;
            int[] Int_FS = { 4 };

            string[] Int_chars = { "^([{])$", "^([}])$", "^([+])$", "^([-])$", "^([0-9])$" };

            

            FA Int_automata = new FA(int_tt, IS, Int_FS, Int_chars);





            // Implementing ID dfa

            int[,] ID_tt = { { 1, 3, 3, 3, 3, 3 }, { 3, 2, 3, 3, 3, 3 }, { 3, 2, 2, 2, 2, 2 }, { 3, 3, 3, 3, 3, 3 } };
            IS = 0;

            int[] ID_FS = { 2 };

            string[] ID_chars = { "^([$])$", "^([A-Za-z])$", "^([0-9])$", "^(_)$", "^(-)$", "^(@)$" };

            FA Id_automata = new FA(ID_tt, IS, ID_FS, ID_chars);


            // implementing String DFA

            int[,] string_TT = { {1,4,4,4,4 }, {3,5,2,2,2 }, {3,5,2,2,2 }, {4,4,4,4,4 }, {4,4,4,4,4 }, {6,2,6,6,6 }, {3,5,2,2,2 } };

            IS = 0;
            int[] string_FS = { 3 };

            string[] string_chars = {"^(~)$", "^(\\\\)$","^([A-Za-z])$","^([0-9])$","^(.)$" };


            FA string_automata = new FA(string_TT, IS, string_FS, string_chars);


            // Finally implemeting the character DFA

            int[,] char_TT = { { 1, 4, 4, 4, 4, 4, 4 }, {  3, 5, 2, 2, 2, 2, 2 }, { 3, 4, 4, 4, 4, 4, 4 }, {  4, 4, 4, 4, 4, 4, 4 }, 
                {  4, 4, 4, 4, 4, 4, 4 }, {  2, 2, 2, 2, 4, 4, 4 } };

            IS = 0;
            int[] char_FS = { 3 };

            string[] char_chars = {  "^(`)$", "^\\\\$", "^(r)$", "^(n)$", "^([0-9])$", "^([A-Za-z])$","^(.)$" };

            FA char_automata = new FA(char_TT, IS, char_FS, char_chars);








            // Validation begin!

            Console.WriteLine("\nGive Input String");

            string s = Console.ReadLine();


            


               if (Id_automata.Validate(s))
               {
                   Console.WriteLine("Valid ID");

               }


               else if (Int_automata.Validate(s))
               {
                   Console.WriteLine("Valid Integer");
               }

               else if (float_automata.Validate(s))
               {
                   Console.WriteLine("Valid float");
               }

              else if (char_automata.Validate(s))
               {
                   Console.WriteLine("Valid Char");
               }
               else if (string_automata.Validate(s))
               {
                   Console.WriteLine("Valid String");
               } 
               else
               {
                   Console.WriteLine("\nInValid");

               }
   





        } // main end
    }

    class FA
    {
        int[,] TT; // Our Transition tabel!
        int IS; // Our Intial State.
        int[] FS; // Our final States;
        string[] chars;


        public FA(int[,] TT, int IS, int[] FS, string[] chars)
        {
            this.TT = TT;
            this.IS = IS;
            this.FS = FS;
            this.chars = chars;

        }


        public Boolean Validate(string input)
        {
            int st = IS;
            int i, j;
            for (i = 0; i < input.Length; i++)
            {
                st = Transition(st, input[i]);
                if (st == -1) // wrong input condition
                {

                    return false;
                }

            }

            for (j = 0; j < FS.Length; j++)
            {
                if (st == FS[j])
                {
                    return true;
                }
            }
            return false;
        }


        public int Transition(int st, char ch)
        {
            int i;



            Regex[] R = new Regex[chars.Length];


            for (i = 0; i < R.Length; i++)
            {
                R[i] = new Regex(chars[i]);
            }
            int j;
            for (j = 0; j < R.Length; j++)
            {

                if (R[j].Match(ch.ToString()).Success)
                {
                    return TT[st, j];
                }
            }


            return -1;

        }



    }

}
