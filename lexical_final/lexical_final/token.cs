﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
 
namespace lexical_final
{
    class token
    {

        string[] keywords = { "repeat","when","dowhen","shift","case","def","create","reverse","break","open","secure","null",
            "empty","whether","further","infurther","beg","static","fin"};

        public string[] DT = { "fixnum", "float", "str", "chr" };

        public static string[] CP= new string [1000];
        public static string[] VP=new string [1000];
        public static int[] line= new int [1000];
        int i = 0;

        public string temp, aa, isString, isChar;
        public string data;
        int count = 1;
        public int charrange;
       
        //****************** File Output **********

        public void write(string wordClass, string word, int line)
        {

            string appendText;

            data = "(" + wordClass + "," + word + "," + line + ")\n";

            appendText = data + Environment.NewLine;
            File.AppendAllText(@"C:\Users\Athar SmaZ Zaib\Desktop\lexical_final\lexical_final\bin\Debug\InputOutput\file.txt", appendText);

        }

        //****************** Input File **********
        public void read()
        {
            int countdot = 0;
            int lineno = 1;
            char ch;
            string temp = null;
            bool check = false;
            charrange = 0;

            //to read character by character data from given path

            StreamReader sr = new StreamReader(@"C:\Users\Athar SmaZ Zaib\Desktop\lexical_final\lexical_final\bin\Debug\InputOutput\Program.cs");
            ch = (char)sr.Read();

            while (!sr.EndOfStream)
            {

                check = false;
                if (ch == '\r')
                {
                    if ((char)sr.Read() == '\n')
                    {
                        lineno++;
                        ch = (char)sr.Read();
                        continue;
                    }
                }


                else if (ch == '\n')
                {
                    lineno++;
                    ch = (char)sr.Read();
                    continue;
                }

                  //****************** String Constant**********

                else if (ch == '"')
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();
                    while ((ch != '"'))
                    {
                        //****************** file end checking **********
                        if (sr.EndOfStream)
                        {
                            line[i] = lineno;
                            break;
                        }

                        if (ch == '\r')
                        {
                            if ((char)sr.Read() == '\n')
                            {
                                lineno++;
                                line[i] = lineno;
                                ch = (char)sr.Read();
                            }
                        }

                        else if (ch == '\n')
                        {
                            lineno++;
                            line[i] = lineno;
                            ch = (char)sr.Read();
                        }

                        if (ch == '\\')
                        {
                            temp = temp + ch;
                            ch = (char)sr.Read();

                            if (ch == '"')
                            {
                                temp = temp + ch;
                                ch = (char)sr.Read();
                                continue;
                            }
                        }

                        temp = temp + ch;
                        ch = (char)sr.Read();
                    }

                    temp = temp + ch;
                    write("StrConstant", temp, lineno);
                    CP[i] = "StrConstant";
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;



                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }
                //****************** Character Constant ************
                else if (ch == '\'')
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();

                    if (ch == '\\')
                    {
                        temp = temp + ch;
                        ch = (char)sr.Read();

                        if (ch == 'n' || ch == 'r' || ch == 't' || ch == '}' || ch == '\\')
                        {
                            if (ch == '\r')
                            {
                                if ((char)sr.Read() == '\n')
                                {
                                    lineno++;
                                }
                            }

                            else if (ch == '\n')
                            {
                                lineno++;
                            }

                            temp = temp + ch;
                            ch = (char)sr.Read();
                        }
                        else
                        {
                            temp = temp + ch;
                            write("LexicalError", temp, lineno);

                            CP[i] = "LexicalError";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;

                            temp = null;
                            ch = (char)sr.Read();
                            continue;
                        }

                        if (ch == '\'')
                        {
                            if (ch == '\r')
                            {
                                if ((char)sr.Read() == '\n')
                                {
                                    lineno++;
                                }
                            }

                            else if (ch == '\n')
                            {
                                lineno++;
                            }

                            temp = temp + ch;
                            write("charConstant", temp, lineno);
                            CP[i] = "charConstant";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;

                            temp = null;
                        }

                        else
                        {
                            temp = temp + ch;
                            write("LexicalError", temp, lineno);
                            CP[i] = "LexicalError";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;
                            temp = null;

                        }

                    }

                    else if (ch == '\'')
                    {
                        if (ch == '\r')
                        {
                            if ((char)sr.Read() == '\n')
                            {
                                lineno++;
                            }
                        }

                        else if (ch == '\n')
                        {
                            lineno++;
                        }

                        temp = temp + ch;
                        write("LexicalError", temp, lineno);
                        CP[i] = "LexicalError";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }


                    else
                    {
                        temp = temp + ch;
                        ch = (char)sr.Read();

                        if (ch == '\'')
                        {
                            temp = temp + ch;
                            write("charConstant", temp, lineno);
                            CP[i] = "charConstant";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;
                            temp = null;
                        }
                        else
                        {
                            temp = temp + ch;
                            write("LexicalError", temp, lineno);
                            CP[i] = "LexicalError";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;
                            temp = null;
                            ch = (char)sr.Read();
                            continue;
                        }

                    }

                    ch = (char)sr.Read();
                    continue;

                }

                //****************** line terminator **********
                else if (ch == ';')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }


                // ****************** Keywords  ********** 
                else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch == '_'))
                {
                    while (ch != ' ')
                    {
                        if (ch == '\r' || ch == '\n' || ch == '`' || ch == ',' || ch == ' ' || ch == '.' || ch == '>' || ch == '<' || ch == '#' || ch == '+' || ch == '-' || ch == '&' || ch == '|' || ch == '=' || ch == '*' || ch == '/' || ch == '%' || ch == '{' || ch == '}' || ch == '[' || ch == ']' || ch == '(' || ch == ')' || ch == '\\' || ch == ':' || ch == ';')
                        {
                            break;
                        }

                        else
                        {
                            temp = temp + ch;
                            ch = (char)sr.Read();
                            if (sr.EndOfStream)
                            {
                                line[i] = lineno;
                                break;
                            }
                        }
                    }


                    for (int x = 0; x < keywords.Length; x++)
                    {
                        if (temp == keywords[x])
                        {
                            write(temp, "-", lineno);
                            CP[i] = temp;
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;
                            temp = null;
                            check = true;
                            break;
                        }
                    }



                    if (check == false)
                    {

                        if (typeCheck(temp) == true)
                        {
                            write("DT", temp, lineno);
                            CP[i] = "DT";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;

                            temp = null;
                            continue;
                        }

                        else if (reCheck(temp) == true)
                        {
                            write("ID", temp, lineno);
                            CP[i] = "ID";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;

                            temp = null;
                            continue;
                        }

                        else
                        {
                            write("LexicalError", temp, lineno);
                            CP[i] = "LexicalError";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;

                            temp = null;
                            continue;
                        }
                    }
                    continue;
                }

                //****************** deci & fixnum **********

                else if ((ch >= '0' && ch <= '9') || ch == '.')
                {
                    countdot = 0;

                    if (ch == '.')
                    {
                        countdot++;
                    }

                    temp = temp + ch;
                    ch = (char)sr.Read();



                    while ((ch >= '0' && ch <= '9') || (ch == '.') || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
                    {
                        if (ch == '.')
                        {
                            countdot++;
                        }

                        if (countdot > 1)
                        {
                            break;
                        }

                        temp = temp + ch;
                        ch = (char)sr.Read();
                    }



                    if (intConst(temp) == true)
                    {
                        write("IntConst", temp, lineno);
                        CP[i] = "IntConst";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        continue;
                    }

                    else if (fltConst(temp) == true)
                    {
                        write("fltConst", temp, lineno);
                        CP[i] = "fltConst";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        continue;
                    }

                    else
                    {
                        write("LexicalError", temp, lineno);
                        CP[i] = "LexicalError";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        continue;
                    }


                }



                        //****************** Relational Operator **********
                else if ((ch == '<'))
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();


                    if (ch == '/')
                    {
                        ch = (char)sr.Read();

                        if (ch == '*')
                        {

                        label1:
                            while (ch != '*')
                            {
                                if (sr.EndOfStream)
                                {
                                    line[i] = lineno;
                                    break;
                                }

                                if (ch == '\r')
                                {
                                    if ((char)sr.Read() == '\n')
                                    {
                                        lineno++;
                                        line[i] = lineno;
                                    }
                                }

                                else if (ch == '\n')
                                {
                                    lineno++;
                                    line[i] = lineno;
                                }

                                ch = (char)sr.Read();
                            }

                            if (sr.EndOfStream)
                            {
                                line[i] = lineno;
                                continue;
                            }

                            ch = (char)sr.Read();

                            if (ch == '/')
                            {
                                ch = (char)sr.Read();
                                continue;

                            }

                            else
                            {
                                goto label1;
                            }

                        }

                    }


                    else if (ch == '=')
                    {
                        temp = temp + ch;
                        write("RelationalOP", temp, lineno);
                        CP[i] = "RelationalOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }

                    else
                    {
                        write("RelationalOP", temp, lineno);
                        CP[i] = "RelationalOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }
                    continue;
                }

                else if ((ch == '>'))
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();

                    if (ch == '>')
                    {
                        while (ch != '\r')
                        {
                            if (sr.EndOfStream)
                            {
                                line[i] = lineno;
                                break;
                            }
                            ch = (char)sr.Read();
                        }

                        temp = null;
                    }

                    else if (ch == '=')
                    {
                        temp = temp + ch;
                        write("RelationalOP", temp, lineno);
                        CP[i] = "RelationalOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }
                    else
                    {
                        write("RelationalOP", temp, lineno);
                        CP[i] = "RelationalOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }
                    continue;
                }

                else if ((ch == '!'))
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();

                    if (ch == '=')
                    {
                        temp = temp + ch;
                        write("RelationalOP", temp, lineno);
                        CP[i] = "RelationalOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }

                    else
                    {
                        write("NOT", temp, lineno);
                        CP[i] = "NOT";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }
                    continue;
                }

                    //****************** Addition & Subtraction **********
                else if ((ch == '+'))
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();

                    if (ch == '+')
                    {
                        temp = temp + ch;
                        write("INCDEC", temp, lineno);
                        CP[i] = "INCDEC";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;


                        temp = null;
                        ch = (char)sr.Read();
                    }
                    else if (ch == '=')
                    {
                        temp = temp + ch;
                        write("AssignOP", temp, lineno);
                        CP[i] = "AssignOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }

                    else
                    {
                        write("AddSub", temp, lineno);
                        CP[i] = "AddSub";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }
                    continue;
                }

                else if ((ch == '-'))
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();

                    if (ch == '-')
                    {
                        temp = temp + ch;
                        write("INCDEC", temp, lineno);
                        CP[i] = "INCDEC";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }

                    else if (ch == '=')
                    {
                        temp = temp + ch;
                        write("AssignOP", temp, lineno);
                        CP[i] = "AssignOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }


                    else if (ch == '<')
                    {
                        ch = (char)sr.Read();

                        if (ch == '=')
                        {
                            temp = temp + ch;
                            write("RelationalOP", temp, lineno);
                            CP[i] = "RelationalOP";
                            VP[i] = temp;
                            line[i] = lineno;
                            i++;

                            temp = null;
                            ch = (char)sr.Read();
                        }

                        temp = temp + ch;
                        write(temp, "-", lineno);
                        CP[i] = temp;
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();


                    }

                    else if (ch == '>')
                    {
                        temp = temp + ch;
                        write(temp, "-", lineno);
                        CP[i] = temp;
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }

                    else
                    {
                        write("AddSub", temp, lineno);
                        CP[i] = "AddSub";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }
                    continue;
                }



                    //****************** Multiplication & Division **********
                else if ((ch == '*'))
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();
                    if (ch == '=')
                    {
                        temp = temp + ch;
                        write("AssignOP", temp, lineno);
                        CP[i] = "AssignOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();
                    }
                    else
                    {
                        write("muldivmod", temp, lineno);
                        CP[i] = "muldivmod";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }
                    continue;

                }

                else if ((ch == '/') || (ch == '%'))
                {
                    temp = temp + ch;
                    write("muldivmod", temp, lineno);
                    CP[i] = "muldivmod";
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }

                    //****************** Equal to **********
                else if ((ch == '='))
                {
                    temp = temp + ch;
                    ch = (char)sr.Read();
                    if (ch == '=')
                    {
                        temp = temp + ch;
                        write("RelationalOP", temp, lineno);
                        CP[i] = "RelationalOP";
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                        ch = (char)sr.Read();

                    }
                    else
                    {
                        write(temp, "-", lineno);
                        CP[i] = temp;
                        VP[i] = temp;
                        line[i] = lineno;
                        i++;

                        temp = null;
                    }
                    continue;
                }
                //****************** Array **********

                else if (ch == '[')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }
                else if (ch == ']')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }
                else if (ch == ',')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }
                else if (ch == '{')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }

                else if (ch == '}')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }
                else if (ch == ' ')
                {
                    ch = (char)sr.Read();
                    continue;
                }

                else if (ch == '(')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;
                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }

                else if (ch == ')')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }

                else if (ch == ':')
                {
                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;
                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }

                else if (ch == ';')
                {

                    temp = temp + ch;
                    write(temp, "-", lineno);
                    CP[i] = temp;
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }


                else
                {
                    temp = temp + ch;
                    write("LexicalError", temp, lineno);
                    CP[i] = "LexicalError";
                    VP[i] = temp;
                    line[i] = lineno;
                    i++;

                    temp = null;
                    ch = (char)sr.Read();
                    continue;
                }
            }
        }

                 public bool reCheck(string isRE)
        {
            Regex regex = new Regex(@"^[a-z]|[A-Z]\w*||_[a-z]|[A-Z]\w*$");


            if (regex.IsMatch(isRE))
            {
                return true;

            }

            else
            {
                return false;

            }
        }

        public bool typeCheck(string isType)
        {

            if (isType == DT[0])
            {
                return true;
            }
            else if (isType == DT[1])
            {
                return true;
            }
            else if (isType == DT[2])
            {
                return true;
            }
            else if (isType == DT[3])
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void print()
        {
            Console.WriteLine(CP[i]);
            Console.WriteLine(VP[i]);

        }
        public bool intConst(string intgr)
        {
            Regex regex = new Regex(@"^\d+$");
            if (regex.IsMatch(intgr))
            {
                return true;
            }

            else
            {
                return false;
            }

        }
        public bool fltConst(string flt)
        {
            Regex regex = new Regex(@"^\d*\.\d+$");

            if (regex.IsMatch(flt))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
              }
          }
       
      
          
        
    

