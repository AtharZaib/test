﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace identifier
{
    class Program
    { 
        static void Main(string[] args)
        {
            Console.WriteLine("Enter any String: ");
            string sc = Console.ReadLine();
            String a=sc;
             if (isValidIdentifier(a))
            {
                Console.WriteLine("Valid!");
            }
            else
            {
                Console.WriteLine("Invalid!");
            }
            Console.ReadKey();
        }
        public static bool isValidIdentifier(String input)
        {
            int initialState = 0;
            int[,] transitionTable = { { 3, 2, 1 }, { 3, 3, 3 }, { 2, 2, 2 }, { 3, 3, 3 } };
            int finalState = 3;
            int i = 0;
            while (i < input.Length)
            {
                char toCheck = input.ElementAt(i);
                if (toCheck == '_')
                {
                    initialState = transitionTable[initialState,0];
                }
                else if ((toCheck >= 'a' && toCheck <= 'z') || (toCheck >= 'A' && toCheck <= 'Z'))
                {
                    initialState = transitionTable[initialState,0];
                }
                else if (toCheck >= '0' && toCheck <= '9')
                {
                    initialState = transitionTable[initialState,1];
                }
                else
                {
                    initialState = 2;
                }
                i++;
            }
            if (initialState == finalState)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
