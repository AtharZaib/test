﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFA
{
    class Program
    {
        static void Main(string[] args)
        { while (true)
            { 
            Console.WriteLine("Enter any String: ");
            string sc = Console.ReadLine();
            String a = sc;

            if (Validate_String(a))
            {
                Console.WriteLine("Valid!");
            }
            else
            {
                Console.WriteLine("Invalid!");
            }
            Console.ReadKey();
        }
    }


        private static int[,] TTstring = { { 5, 5, 5, 5, 1 }, { 2, 2, 3, 3, 4 }, { 2, 2, 3, 3, 4 }, { 3, 2, 2, 2, 4 }, { 5, 5, 5, 5, 5 }, { 5, 5, 5, 5, 5 } };
        static int IS = 0;
        public static bool Validate_String(String inp)
        {
            int state = IS;
            int FSstring = 4;
            for (int a = 0; a < inp.Length; a++)
            {
                state = Transition_String(state, inp.ElementAt(a));
                if (state > 6)
                {
                    return false;
                }
            }
            if (state == FSstring)
            {
                return true;
            }
            return false;

        }
        private static int Transition_String(int state, char ch)
        {
            int inp;

            if (ch == 'a' || ch == 'b' || ch == 'n' || ch == 'r' || ch == 't' || ch == 'v' || ch == 'f')
            {
                inp = 1;
            }
            else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
            {
                inp = 0;
            }
            else if (ch != '\\' && ch != '\"')
            {
                inp = 0;
            }
            else if (ch == '\\')
            {
                inp = 2;
            }
            else if (ch == '\'')
            {
                inp = 3;
            }
            else if (ch == '\"')
            {
                inp = 4;
            }
            else
            {
                return 7;
            }
            return TTstring[state, inp];
        }


    }
}
